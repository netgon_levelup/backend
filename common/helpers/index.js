module.exports = {
  getApp(model) {
    return new Promise((resolve, reject) => {
      model.getApp((err, app) => {
        if (err)
          reject(err);
        else
          resolve(app);
      });
    });
  },
};
