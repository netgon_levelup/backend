const { getApp } = require('../helpers/');
const { ROLES, GENDERS } = require('../helpers/constants');

module.exports = async (User) => {
  const app = await getApp(User);
  // User.validatesInclusionOf('realm',  { in: ROLES });
  // User.validatesInclusionOf('gender', { in: GENDERS });

  User.beforeRemote('create', async (ctx) => {
    const details = ctx.args.data.details;
    if (details !== undefined) {
      const result = await app.models.UserDetails.create({
        firstName: details.firstName,
        lastName: details.lastName,
        middleName: details.middleName,
        avatar: details.avatar,
      });
      ctx.args.data.userDetailsId = result.id;
    }
  });

  User.me = (req, cb) => {
    cb(null, req.accessToken);
  };
  User.remoteMethod('me', {
    accepts: { arg: 'req', type: 'object', http: { source: 'req' } },
    returns: { type: 'object', root: true },
    http: { path: '/me', verb: 'get' },
    description: 'find AccessToken object by provided token',
  });
};
