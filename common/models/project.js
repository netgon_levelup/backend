const { NEW, PROJECT_STATUS } = require('../helpers/constants');
const { getApp } = require('../helpers/');

module.exports = async function(Project) {
  const app = await getApp(Project);
  Project.validatesInclusionOf('status', { in: Object.values(PROJECT_STATUS) });

  Project.afterRemote('create', (ctx, user, next) => {
    let promises = [];
    const project = ctx.result;
    const { skills, files } = ctx.args.data;
    if (skills !== undefined && Array.isArray(skills)) {
      skills.forEach(async (item) => {
        await app.models.ProjectSkills.create({
          skillId: item,
          projectId: project.id,
        });
      });
    }
    if (files !== undefined && Array.isArray(files)) {
      files.forEach(async (item) => {
        await app.models.ProjectFiles.create({
          name: item,
          projectId: project.id,
        });
      });
    }
    next();
  });
};
