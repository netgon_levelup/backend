const uuid = require('uuid/v4');

module.exports = function(app) {
  app.dataSources.storage.connector.getFilename = (origFilename, req, res) => {
    const filename = origFilename.name;
    const parts = filename.split('.');
    const extension = parts[parts.length - 1];
    const newFilename = `${uuid()}.${extension}`;
    return newFilename;
  };
};
