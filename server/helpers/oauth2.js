const lbOAuth2 = require('loopback-component-oauth2');

module.exports = (app) => {
  const options = {
    dataSource: app.dataSources.db, // Data source for oAuth2 metadata persistence
    loginPage: '/login', // The login page url
    loginPath: '/login',  // The login form processing url
  };

  lbOAuth2.oAuth2Provider(
    app, // The app instance
    options // The options
  );
}