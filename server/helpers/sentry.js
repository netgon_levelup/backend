const raven = require('raven');

module.exports = (app) => {
  raven.config(app.get('sentry')).install();

  app.sentry = raven;

  const captureException = (err) => { raven.captureException(err); };

  process.on('uncaughtException',   captureException);
  process.on('unhandledRejection',  captureException);
};
