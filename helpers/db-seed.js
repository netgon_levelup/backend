const app = require('../server/server');

const Migrate = app.models.Migration;

const dataSource = app.dataSources.localdb;

dataSource.automigrate((err) => {
  Migrate.migrate('up', (err) => {
    console.log(err);
    process.exit(0);
  });
});
