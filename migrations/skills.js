const skills = [
  'PHP',
  'Java',
  'CSS/HTML',
  'Golang',
  'JavaScript',
  'Adobe Photoshop',
  'WebSocket',
  'AWS',
  'Azure',
  'Docker',
]
const createSkill = async (dataSource, data) => {
  const skill = await dataSource.models.skill.create({
    name: data,
  });

  return await skill.save();
};

module.exports = {
  up: async (dataSource, next) => {
    skills.forEach(item => createSkill(dataSource, item));
    next();
  },
  down: async (dataSource, next) => {
    await dataSource.models.skill.destroyAll();
    next();
  },
};
