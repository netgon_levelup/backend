const dummyAdmin = {
  password: 'q1w2e3r4',
  email: 'admin@dummy.com',
  username: 'admin',
  details: {
    avatar: 'default-avatar-knives-ninja.png',
    firstName: 'Dummy',
    lastName: 'Admin',
    middleName: 'Adminov',
  },
};

let createUser = async (dataSource, data, roleName) => {
  const { firstName, lastName, middleName, avatar } = data.details;
  const details = await dataSource.models.userDetails.create({
    avatar,
    firstName,
    lastName,
    middleName,
  });
  const { email, username, password } = data;
  const user = await dataSource.models.user.create({
    email,
    username,
    password,
    userDetailsId: details.id.toString(),
  });

  return await user.save();
};

module.exports = {
  up: async (dataSource, next) => {
    await createUser(dataSource, dummyAdmin, 'admin');
    next();
  },
  down: async (dataSource, next) => {
    await dataSource.models.user.destroyAll();
    next();
  },
};
