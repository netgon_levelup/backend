const categories = [
  'Дизайн',
  'Программирование',
  'Копирайтинг',
]
const createCategory = async (dataSource, data) => {
  const category = await dataSource.models.category.create({
    name: data,
  });

  return await category.save();
};

module.exports = {
  up: async (dataSource, next) => {
    categories.forEach(item => createCategory(dataSource, item));
    next();
  },
  down: async (dataSource, next) => {
    await dataSource.models.category.destroyAll();
    next();
  },
};
